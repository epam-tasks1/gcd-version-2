﻿using System;

namespace Gcd
{
    public static class IntegerExtensions
    {
        public static int ReturnGCDByEuclidean(int a, int b)
        {
            a = Math.Abs(a);
            b = Math.Abs(b);

            if (a == 0 && b == 0)
            {
                return 0;
            }

            if (a == 0)
            {
                return b;
            }

            if (b == 0)
            {
                return a;
            }

            if (a == b)
            {
                return a;
            }

            if (a < b)
            {
                (a, b) = (b, a);
            }

            int remainderOfDivision;
            int divisionNumber = b;
            int dividerNumber = a;
            bool checkСondition = true;

            while (checkСondition)
            {
                remainderOfDivision = dividerNumber % divisionNumber;

                if (remainderOfDivision == 0)
                {
                    return b;
                }

                dividerNumber = (dividerNumber - remainderOfDivision) / (dividerNumber / divisionNumber);
                divisionNumber = remainderOfDivision;

                if (dividerNumber % remainderOfDivision == 0)
                {
                    checkСondition = false;
                }
            }

            return divisionNumber;
        }

        public static bool CheckForZeros(int[] nums)
        {
            if (nums == null)
            {
                return false;
            }

            for (int i = 0; i < nums.Length; i++)
            {
                if (nums[i] != 0)
                {
                    return false;
                }
            }

            return true;
        }

        public static int ReturnGCDByStein(int a, int b)
        {
            a = Math.Abs(a);
            b = Math.Abs(b);

            if (a == b)
            {
                return a;
            }

            if (a == 0)
            {
                return b;
            }

            if (b == 0)
            {
                return a;
            }

            int coefficient = 0;

            while (a != b)
            {
                if (a % 2 != 0)
                {
                    if (b % 2 == 0)
                    {
                        b >>= 1;
                        continue;
                    }

                    if (a > b)
                    {
                        a = (a - b) >> 1;

                        continue;
                    }

                    b = (b - a) >> 1;
                }
                else
                {
                    if (b % 2 == 0)
                    {
                        coefficient++;
                        a >>= 1;
                        b >>= 1;
                        continue;
                    }

                    if (b % 2 != 0)
                    {
                        a >>= 1;
                    }
                }
            }

            return a << coefficient;
        }

        public static int GetGcdByEuclidean(int a, int b)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            return ReturnGCDByEuclidean(a, b);
        }

        public static int GetGcdByEuclidean(int a, int b, int c)
        {
            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue || c == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            return ReturnGCDByEuclidean(ReturnGCDByEuclidean(a, b), c);
        }

        public static int GetGcdByEuclidean(int a, int b, params int[] other)
        {
            if (other.Length == 0)
            {
                if (a == 0 && b == 0)
                {
                    throw new ArgumentException("All numbers are 0 at the same time.");
                }

                if (a == int.MinValue || b == int.MinValue)
                {
                    throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
                }

                return ReturnGCDByEuclidean(a, b);
            }

            if (a == 0 && b == 0 && CheckForZeros(other))
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            int result = other[0];

            for (int i = 1; i < other.Length; i++)
            {
                result = ReturnGCDByEuclidean(result, other[i]);
            }

            return ReturnGCDByEuclidean(ReturnGCDByEuclidean(a, b), result);
        }

        public static int GetGcdByStein(int a, int b)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            return ReturnGCDByStein(a, b);
        }

        public static int GetGcdByStein(int a, int b, int c)
        {
            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue || c == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            return ReturnGCDByStein(ReturnGCDByStein(a, b), c);
        }

        public static int GetGcdByStein(int a, int b, params int[] other)
        {
            if (other.Length == 0)
            {
                if (a == 0 && b == 0)
                {
                    throw new ArgumentException("All numbers are 0 at the same time.");
                }

                if (a == int.MinValue || b == int.MinValue)
                {
                    throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
                }

                return ReturnGCDByEuclidean(a, b);
            }

            if (a == 0 && b == 0 && CheckForZeros(other))
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            int result = other[0];

            for (int i = 1; i < other.Length; i++)
            {
                result = ReturnGCDByEuclidean(result, other[i]);
            }

            return ReturnGCDByStein(ReturnGCDByStein(a, b), result);
        }

        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b)
        {
            elapsedTicks = 0;

            if (a == 0 && b == 0)
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a is int.MinValue || b is int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            return ReturnGCDByEuclidean(a, b);
        }

        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b, int c)
        {
            elapsedTicks = 0;

            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a is int.MinValue || b is int.MinValue || c is int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            return ReturnGCDByEuclidean(ReturnGCDByEuclidean(a, b), c);
        }

        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b, params int[] other)
        {
            elapsedTicks = 0;

            if (other.Length == 0)
            {
                if (a == 0 && b == 0)
                {
                    throw new ArgumentException("All numbers are 0 at the same time.");
                }

                if (a == int.MinValue || b == int.MinValue)
                {
                    throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
                }

                return ReturnGCDByEuclidean(a, b);
            }

            if (a == 0 && b == 0 && CheckForZeros(other))
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            int result = other[0];

            for (int i = 1; i < other.Length; i++)
            {
                result = ReturnGCDByEuclidean(result, other[i]);
            }

            return ReturnGCDByEuclidean(ReturnGCDByEuclidean(a, b), result);
        }

        public static int GetGcdByStein(out long elapsedTicks, int a, int b)
        {
            elapsedTicks = 0;

            if (a == 0 && b == 0)
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            return ReturnGCDByStein(a, b);
        }

        public static int GetGcdByStein(out long elapsedTicks, int a, int b, int c)
        {
            elapsedTicks = 0;

            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue || c is int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            return ReturnGCDByStein(ReturnGCDByStein(a, b), c);
        }

        public static int GetGcdByStein(out long elapsedTicks, int a, int b, params int[] other)
        {
            elapsedTicks = 0;

            if (other.Length == 0)
            {
                if (a == 0 && b == 0)
                {
                    throw new ArgumentException("All numbers are 0 at the same time.");
                }

                if (a == int.MinValue || b == int.MinValue)
                {
                    throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
                }

                return ReturnGCDByEuclidean(a, b);
            }

            if (a == 0 && b == 0 && CheckForZeros(other))
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            int result = other[0];

            for (int i = 1; i < other.Length; i++)
            {
                result = ReturnGCDByEuclidean(result, other[i]);
            }

            return ReturnGCDByStein(ReturnGCDByStein(a, b), result);
        }
    }
}
